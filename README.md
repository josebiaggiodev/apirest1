# API Rest One

# Sobre o projeto

Uma API Rest que consiste numa listagem de produtos.

# Diagrama de classes em UML

![Diagrama](https://gitlab.com/josebiaggiodev/assets/-/raw/main/classDiagramAPIRestOne.png)

# Tecnologias

- Java 17
- Spring Boot 3.0.6
- Maven 4.0.0
- H2 Database

# Autor

José Biaggio