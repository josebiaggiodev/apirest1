INSERT INTO tb_department (name) VALUES ('Electronics');
INSERT INTO tb_department (name) VALUES ('Clothing');

INSERT INTO tb_product (name, price, department_id) VALUES ('Macbook', 5000.0, 1);
INSERT INTO tb_product (name, price, department_id) VALUES ('PC Gamer', 3000.0, 1);
INSERT INTO tb_product (name, price, department_id) VALUES ('Hat', 100.0, 2);
