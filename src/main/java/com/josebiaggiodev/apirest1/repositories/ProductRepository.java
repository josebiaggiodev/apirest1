package com.josebiaggiodev.apirest1.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.josebiaggiodev.apirest1.entities.Product;

public interface ProductRepository extends JpaRepository<Product, Long>{
    
}
