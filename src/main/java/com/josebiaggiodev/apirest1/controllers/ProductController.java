package com.josebiaggiodev.apirest1.controllers;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.josebiaggiodev.apirest1.entities.Department;
import com.josebiaggiodev.apirest1.entities.Product;
import com.josebiaggiodev.apirest1.repositories.ProductRepository;

@RestController
@RequestMapping(value = "/products")
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @GetMapping
    public List<Product> getDepartment() {
        List<Product> list = productRepository.findAll();

        return list;
    }
}
